DROP TABLE IF EXISTS countries;

CREATE TABLE countries (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  current_president VARCHAR(250) NOT NULL,
  currency varchar2(3) NOT NULL,
  code number not null,
  population_total number not null,
  alternate_name varchar(250) not null
);

INSERT INTO countries (name, current_president, currency, code, population_total, alternate_name) VALUES
  ('United States', 'Biden', 'USD', 1, 350, 'USA'),
  ('Colombia', 'Duque', 'COP', 57, 50, 'LOCOMBIA'),
  ('Canada', 'Trudeau', 'CAD', 1, 50, 'COLD');
