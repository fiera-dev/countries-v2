package com.fieraservices.countries;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="countries")
@Data
public class Country {
    @Id
    private Long id;

    @Column
    private String name;

    @Column
    private String currentPresident;

    @Column
    private String currency;

    @Column
    private Long code;

    @Column
    private Long populationTotal;

    @Column
    private String alternateName;
}
